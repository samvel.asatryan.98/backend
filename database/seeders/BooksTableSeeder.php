<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Book;


class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $books = [];
      $data = require base_path('constants/BooksData.php');
      foreach($data as $item) {
          $books[] = [
            'title' => $item['title'],
            'image' => $item['imageUrl'],
            'rating' => $item['avgRating'],
            'ratings_count' => $item['ratingsCount'],
            'author' => $item['author']['name'],
            'description' => $item['description']['html'],
          ];
      }
      Book::insert($books);
    }
}


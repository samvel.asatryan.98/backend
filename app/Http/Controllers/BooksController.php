<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BooksController extends Controller
{
    public function index(Request $request)
    {
        $searchTerm = $request->query('search');
        $books = Book::select('*')
            ->where(function ($query) use ($searchTerm) {
                $query->where('title', 'like', "%{$searchTerm}%")
                    ->orWhere('author', 'like', "%{$searchTerm}%");
            })
            ->orderByRaw("CASE WHEN title LIKE 'lov%' THEN 1 ELSE 0 END")
            ->orderBy('title', 'asc')
            ->get();
        return response()->json($books);
    }
}
